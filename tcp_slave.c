/* FreeModbus Slave Example ESP32

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "esp_err.h"
#include "sdkconfig.h"
#include "esp_log.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "mdns.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "mbcontroller.h"       // for mbcontroller defines and api
#include "modbus_params.h"      // for modbus parameters structures

#include "driver/gpio.h"        // for led control connected to GPIO

#define MB_TCP_PORT_NUMBER      (CONFIG_FMB_TCP_PORT_DEFAULT)
#define MB_MDNS_PORT            (502)

// Defines below are used to define register start address for each type of Modbus registers
#define MB_REG_DISCRETE_INPUT_START         (0x0000)
#define MB_REG_INPUT_START                  (0x0000)
#define MB_REG_HOLDING_START                (0x0000)
#define MB_REG_COILS_START                  (0x0000)

#define MB_PAR_INFO_GET_TOUT                (10) // Timeout for get parameter info
#define MB_CHAN_DATA_MAX_VAL                (10)
#define MB_CHAN_DATA_OFFSET                 (1.1f)

#define MB_READ_MASK                        (MB_EVENT_INPUT_REG_RD \
                                                | MB_EVENT_HOLDING_REG_RD \
                                                | MB_EVENT_DISCRETE_RD \
                                                | MB_EVENT_COILS_RD)
#define MB_WRITE_MASK                       (MB_EVENT_HOLDING_REG_WR \
                                                | MB_EVENT_COILS_WR)
#define MB_READ_WRITE_MASK                  (MB_READ_MASK | MB_WRITE_MASK)

#define SLAVE_TAG "SLAVE_TEST"

static portMUX_TYPE param_lock = portMUX_INITIALIZER_UNLOCKED;

// This is enumeration for actions performed by slave device
typedef enum
{
    DEV_ACTION_DEFAULT = 0x00,   /*!< Device action default. */
    DEV_ACTION_LAMP_OFF = 0x00,  /*!< Device action default. */
    DEV_ACTION_LAMP_ON,          /*!< Device action switch lamp on. */
    DEV_ACTION_OPT_MAX
} device_action_opt_t;

/*!< Device action correspond LEDs to blink (LAMP) . */
typedef enum
{
    DEV_LED_ACTION_0 = 0,
    DEV_LED_ACTION_1 = 2,
    DEV_LED_ACTION_2 = 4,
    DEV_LED_ACTION_3 = 5,
    DEV_LED_ACTION_MAX
} device_action_leds_t;

typedef enum
{
    DEV_ACTION_0,
    DEV_ACTION_1,
    DEV_ACTION_2,
    DEV_ACTION_3,
    DEV_ACTION_4,
    DEV_ACTION_5,
    DEV_ACTION_6,
    DEV_ACTION_7,
    DEV_ACTION_8,
    DEV_ACTION_MAX
} device_action_regs_t;

#define GET_FIELD_OFFSET(field) ((uint16_t)(offsetof(holding_reg_params_t, field)) + 1)

#define GET_ADDR_OFFSET(field_addr) ( ( ((uint32_t)field_addr >= (uint32_t)&holding_reg_params) && \
                                        ((uint32_t)field_addr <= ((uint32_t)&holding_reg_params + sizeof(holding_reg_params)) ) ) ? \
                                                (((uint32_t)field_addr - (uint32_t)&holding_reg_params) + 1) : 0 \
                                    )
#define IS_FIELD_IN_RANGE(start, size, field, val) ( __extension__( {bool in_range = (GET_ADDR_OFFSET(start) <= GET_FIELD_OFFSET(field)) && \
                                                        (GET_ADDR_OFFSET((uint32_t)start + (size << 1) - 1) >= GET_FIELD_OFFSET(field)); \
                                                        val = in_range ? holding_reg_params.field : 0; in_range;}) )

#if CONFIG_MB_MDNS_IP_RESOLVER

#define MB_ID_BYTE0(id) ((uint8_t)(id))
#define MB_ID_BYTE1(id) ((uint8_t)(((uint16_t)(id) >> 8) & 0xFF))
#define MB_ID_BYTE2(id) ((uint8_t)(((uint32_t)(id) >> 16) & 0xFF))
#define MB_ID_BYTE3(id) ((uint8_t)(((uint32_t)(id) >> 24) & 0xFF))

#define MB_ID2STR(id) MB_ID_BYTE0(id), MB_ID_BYTE1(id), MB_ID_BYTE2(id), MB_ID_BYTE3(id)

#if CONFIG_FMB_CONTROLLER_SLAVE_ID_SUPPORT
#define MB_DEVICE_ID (uint32_t)CONFIG_FMB_CONTROLLER_SLAVE_ID
#endif

#define MB_SLAVE_ADDR (CONFIG_MB_SLAVE_ADDR)

#define MB_MDNS_INSTANCE(pref) pref"mb_slave_tcp"

// convert mac from binary format to string
static inline char* gen_mac_str(const uint8_t* mac, char* pref, char* mac_str)
{
    sprintf(mac_str, "%s%02X%02X%02X%02X%02X%02X", pref, MAC2STR(mac));
    return mac_str;
}

static inline char* gen_id_str(char* service_name, char* slave_id_str)
{
    sprintf(slave_id_str, "%s%02X%02X%02X%02X", service_name, MB_ID2STR(MB_DEVICE_ID));
    return slave_id_str;
}

static inline char* gen_host_name_str(char* service_name, char* name)
{
    sprintf(name, "%s_%02X", service_name, MB_SLAVE_ADDR);
    return name;
}

static void start_mdns_service()
{
    char temp_str[32] = {0};
    uint8_t sta_mac[6] = {0};
    ESP_ERROR_CHECK(esp_read_mac(sta_mac, ESP_MAC_WIFI_STA));
    char* hostname = gen_host_name_str(MB_MDNS_INSTANCE(""), temp_str);
    //initialize mDNS
    ESP_ERROR_CHECK(mdns_init());
    //set mDNS hostname (required if you want to advertise services)
    ESP_ERROR_CHECK(mdns_hostname_set(hostname));
    ESP_LOGI(SLAVE_TAG, "mdns hostname set to: [%s]", hostname);

    //set default mDNS instance name
    ESP_ERROR_CHECK(mdns_instance_name_set(MB_MDNS_INSTANCE("esp32_")));

    //structure with TXT records
    mdns_txt_item_t serviceTxtData[] = {
        {"board","esp32"}
    };

    //initialize service
    ESP_ERROR_CHECK(mdns_service_add(hostname, "_modbus", "_tcp", MB_MDNS_PORT, serviceTxtData, 1));
    //add mac key string text item
    ESP_ERROR_CHECK(mdns_service_txt_item_set("_modbus", "_tcp", "mac", gen_mac_str(sta_mac, "\0", temp_str)));
    //add slave id key txt item
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_modbus", "_tcp", "mb_id", gen_id_str("\0", temp_str)));
}

#endif

// Set register values into known state
static void setup_reg_data(void)
{
    // Define initial state of parameters
    discrete_reg_params.device_action_state0 = 1;
    discrete_reg_params.device_action_state1 = 0;
    discrete_reg_params.device_action_state2 = 0;
    discrete_reg_params.device_action_state3 = 0;
    discrete_reg_params.device_action_state4 = 0;
    discrete_reg_params.device_action_state5 = 0;
    discrete_reg_params.device_action_state6 = 0;
    discrete_reg_params.device_action_state7 = 0;
    discrete_reg_params.device_action_state8 = 0;
    discrete_reg_params.device_action_state9 = 0;

    holding_reg_params.device_action_0 = DEV_ACTION_LAMP_ON; // Set by default to predefined state
    holding_reg_params.device_action_1 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_2 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_3 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_4 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_5 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_6 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_7 = DEV_ACTION_DEFAULT;
    holding_reg_params.device_action_8 = DEV_ACTION_DEFAULT;
}

// init action leds to initial state
static void init_action_leds(void)
{
    gpio_reset_pin(DEV_LED_ACTION_0);
    gpio_set_direction(DEV_LED_ACTION_0, GPIO_MODE_OUTPUT);
    gpio_set_level(DEV_LED_ACTION_0, 1); // set led by default
    gpio_reset_pin(DEV_LED_ACTION_1);
    gpio_set_direction(DEV_LED_ACTION_1, GPIO_MODE_OUTPUT);
    gpio_reset_pin(DEV_LED_ACTION_2);
    gpio_set_direction(DEV_LED_ACTION_2, GPIO_MODE_OUTPUT);
}

// An example application of Modbus slave. It is based on freemodbus stack.
// See deviceparams.h file for more information about assigned Modbus parameters.
// These parameters can be accessed from main application and also can be changed
// by external Modbus master host.
void app_main(void)
{
    esp_err_t result = nvs_flash_init();
    if (result == ESP_ERR_NVS_NO_FREE_PAGES || result == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      result = nvs_flash_init();
    }
    ESP_ERROR_CHECK(result);
    esp_netif_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

#if CONFIG_MB_MDNS_IP_RESOLVER
    start_mdns_service();
#endif

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    ESP_ERROR_CHECK(esp_wifi_set_ps(WIFI_PS_NONE));

    // Set UART log level
    esp_log_level_set(SLAVE_TAG, ESP_LOG_INFO);
    void* mbc_slave_handler = NULL;

    ESP_ERROR_CHECK(mbc_slave_init_tcp(&mbc_slave_handler)); // Initialization of Modbus controller
    
    mb_param_info_t reg_info; // keeps the Modbus registers access information
    mb_register_area_descriptor_t reg_area; // Modbus register area descriptor structure
    
    mb_communication_info_t comm_info = { 0 };
    comm_info.ip_port = MB_TCP_PORT_NUMBER;
#if !CONFIG_EXAMPLE_CONNECT_IPV6
    comm_info.ip_addr_type = MB_IPV4;
#else
    comm_info.ip_addr_type = MB_IPV6;
#endif
    comm_info.ip_mode = MB_MODE_TCP;
    comm_info.ip_addr = NULL;
    comm_info.ip_netif_ptr = (void*)get_example_netif();
    // Setup communication parameters and start stack
    ESP_ERROR_CHECK(mbc_slave_setup((void*)&comm_info));

    // The code below initializes Modbus register area descriptors
    // for Modbus Holding Registers, Input Registers, Coils and Discrete Inputs
    // Initialization should be done for each supported Modbus register area according to register map.
    // When external master trying to access the register in the area that is not initialized
    // by mbc_slave_set_descriptor() API call then Modbus stack
    // will send exception response for this register area.
    reg_area.type = MB_PARAM_HOLDING; // Set type of register area
    reg_area.start_offset = MB_REG_HOLDING_START; // Offset of register area in Modbus protocol
    reg_area.address = (void*)&holding_reg_params; // Set pointer to storage instance
    reg_area.size = sizeof(holding_reg_params); // Set the size of register storage instance
    ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area));

    // Initialization of Input Registers area
    reg_area.type = MB_PARAM_INPUT;
    reg_area.start_offset = MB_REG_INPUT_START;
    reg_area.address = (void*)&input_reg_params;
    reg_area.size = sizeof(input_reg_params);
    ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area));

    // Initialization of Coils register area
    reg_area.type = MB_PARAM_COIL;
    reg_area.start_offset = MB_REG_COILS_START;
    reg_area.address = (void*)&coil_reg_params;
    reg_area.size = sizeof(coil_reg_params);
    ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area));

    // Initialization of Discrete Inputs register area
    reg_area.type = MB_PARAM_DISCRETE;
    reg_area.start_offset = MB_REG_DISCRETE_INPUT_START;
    reg_area.address = (void*)&discrete_reg_params;
    reg_area.size = sizeof(discrete_reg_params);
    ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area));

    setup_reg_data(); // Set values into known state

    // Starts of modbus controller and stack
    ESP_ERROR_CHECK(mbc_slave_start());

    ESP_LOGI(SLAVE_TAG, "Modbus slave stack initialized.");
    ESP_LOGI(SLAVE_TAG, "Start modbus test...");

    // Init action LEDs to initial state before control
    init_action_leds();

    // The cycle below will be terminated when parameter holding_data0
    // incremented each access cycle reaches the CHAN_DATA_MAX_VAL value.
    for(;;) {
        // Check for read/write events of Modbus master for certain events
        mb_event_group_t event = mbc_slave_check_event(MB_READ_WRITE_MASK);
        const char* rw_str = (event & MB_READ_MASK) ? "READ" : "WRITE";
        // Filter events and process them accordingly
        if (event & (MB_EVENT_HOLDING_REG_WR | MB_EVENT_HOLDING_REG_RD)) {
            // Get parameter information from parameter queue
            ESP_ERROR_CHECK(mbc_slave_get_param_info(&reg_info, MB_PAR_INFO_GET_TOUT));
            ESP_LOGI(SLAVE_TAG, "HOLDING %s (%u us), ADDR:%u, TYPE:%u, INST_ADDR:0x%.4x, SIZE:%u",
                    rw_str,
                    (uint32_t)reg_info.time_stamp,
                    (uint32_t)reg_info.mb_offset,
                    (uint32_t)reg_info.type,
                    (uint32_t)reg_info.address,
                    (uint32_t)reg_info.size);
            // a variable that keeps the value of the action when being accessed
            uint16_t action_value = *(uint16_t*)reg_info.address;
            // Check if the holding register structure field being accessed my host
            if (IS_FIELD_IN_RANGE(reg_info.address, reg_info.size, device_action_0, action_value)) {
                ESP_LOGI(SLAVE_TAG, "Handle ACTION_0 %s , value = %d.", rw_str, action_value);
                // Check the action value and do associated action (set/reset led pin currently)
                // The register can include more actions instead of just switch led on/off (handle these actions appropriately later)
                if (action_value == DEV_ACTION_LAMP_ON) {
                    // Switch lamp on
                    // The state of lamp is available as discrete inputs over Modbus protocol
                    portENTER_CRITICAL(&param_lock);
                    discrete_reg_params.device_action_state0 = 1;
                    // Change the value inside critical section if required.
                    portEXIT_CRITICAL(&param_lock);
                    gpio_set_level(DEV_LED_ACTION_0, 1);
                } else if (action_value == DEV_ACTION_LAMP_OFF) {
                    // Switch lamp off
                    discrete_reg_params.device_action_state0 = 0;
                    gpio_set_level(DEV_LED_ACTION_0, 0);
                }
            }
            // The same way check other fields in range of read/wrote registers
            if (IS_FIELD_IN_RANGE(reg_info.address, reg_info.size, device_action_1, action_value)) {
                ESP_LOGI(SLAVE_TAG, "Handle ACTION_1 %s , value = %d.", rw_str, action_value);
                if (action_value == DEV_ACTION_LAMP_ON) {
                    // Switch lamp on
                    discrete_reg_params.device_action_state1 = 1;
                    gpio_set_level(DEV_LED_ACTION_1, 1);
                } else if (action_value == DEV_ACTION_LAMP_OFF) {
                    // Switch lamp off
                    discrete_reg_params.device_action_state1 = 0;
                    gpio_set_level(DEV_LED_ACTION_1, 0);
                }
            }
            if (IS_FIELD_IN_RANGE(reg_info.address, reg_info.size, device_action_2, action_value)) {
                ESP_LOGI(SLAVE_TAG, "Handle ACTION_2 %s , value = %d.", rw_str, action_value);
                if (action_value == DEV_ACTION_LAMP_ON) {
                    // Switch lamp on
                    discrete_reg_params.device_action_state2 = 1;
                    gpio_set_level(DEV_LED_ACTION_2, 1);
                } else if (action_value == DEV_ACTION_LAMP_OFF) {
                    // Switch lamp off
                    discrete_reg_params.device_action_state2 = 0;
                    gpio_set_level(DEV_LED_ACTION_2, 0);
                }
            }
            if (IS_FIELD_IN_RANGE(reg_info.address, reg_info.size, device_action_9, action_value)) {
                // Exit condition: device_action_9
                break;
            }
            // This is how to safely change fields in the Modbus structure that being accessed by Modbus driver
            portENTER_CRITICAL(&param_lock);
            holding_reg_params.device_action_last = action_value; // Keep last action
            portEXIT_CRITICAL(&param_lock);

            // Do the same to filter other actions accordingly
        }
    }
    // Destroy of Modbus controller on alarm
    ESP_LOGI(SLAVE_TAG,"Modbus controller destroyed.");
    vTaskDelay(100);
    ESP_ERROR_CHECK(mbc_slave_destroy());
#if CONFIG_MB_MDNS_IP_RESOLVER
    mdns_free();
#endif
}
